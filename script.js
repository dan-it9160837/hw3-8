"use strict" ;

// 1. Опишіть своїми словами як працює метод forEach.

// Метод forEach використовується для перебору елементів в масиві. Він приймає функцію зворотного виклику, ця функція отримує три аргументи: поточний елемент, індекс поточного елемента та сам масив.

// 2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

// Методи, які мутують існуючий масив: push, unshift, pop, shift, splice.
// Методи, які повертають новий масив: slice, concat, map, filter, reduce ,sort ,reverse , every, some, includes, find, indexof, findindex

// 1.push()
// let cars = ["Toyota","Honda","BMW"];
// cars.push("Audi");
// console.log(cars);

// 2.unshift()
// let cars = ["Honda","BMW","Audi"];
// cars.unshift("Toyota");
// console.log(cars);

// 3.pop()
// let cars = ["Toyota","Honda","BMW","Audi"];
// cars.pop()
// console.log(cars);

// 1.slice()
// let cars = ["Toyota","Honda","BMW","Audi","Volvo"];
// let cars2=cars.slice(1);
// console.log(cars2) ;

// 2.concat()
// let cars = ["Toyota","Honda","BMW","Audi"];
// let bikes = ["Yamaha", "Suzuki"];
// let vehicles = cars.contact(bikes);
// console.log(vehicles);

// 3.map()
// let num1 = [2,3,4,5,6,7];
// let num2 = num1.map(double);
// function double(value){
//     return value *2 ;
// }
// console.log(num2);


// 3. Як можна перевірити, що та чи інша змінна є масивом?

// Можна використати Array.isArray()
// let myArray = [1, 2, 3];
// if (Array.isArray(myArray)) {
//   console.log('Це масив!');
// } else {
//   console.log('Це не масив.');
// }

// Використання instanceof Array:
// let myArray = [1, 2, 3];
// if (myArray instanceof Array) {
//   console.log('Це масив!');
// } else {
//   console.log('Це не масив.');
// }


// 4. В яких випадках краще використовувати метод map(), а в яких forEach()?
// мета map() - створити новий масив, який складається з результатів виклику функції для кожного елемента початкового масиву.
// forEach() використовується для виконання функції для кожного елемента масиву, але він не створює нового масиву.
// map() не змінює оригінальний масив; він повертає новий масив результатів.
// forEach() не повертає значень; він просто використовує функцію для кожного елемента.
// Отже, якщо вам потрібно отримати новий масив на основі обробки кожного елемента, то map() - це кращий вибір. Якщо вам просто потрібно виконати дії для кожного елемента без створення нового масиву, то forEach() може бути зручнішим.


// 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.

// const strings = ["travel", "hello", "eat", "ski", "lift"];
// const count = strings.filter(s => s.length > 3).length;
// console.log(count);

// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}.
//  Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
// Відфільтрований масив виведіть в консоль.

// const people = [
//     { name: "Artem", age: 22, sex: "male" },
//     { name: "Sofiia", age: 21, sex: "female" },
//     { name: "Viktor", age: 48, sex: "male" },
//     { name: "Viktoriia", age: 50, sex: "female" },
//    ];
//       const filteredPeople = people.filter(person => person.sex === "male");
//       console.log(filteredPeople);
